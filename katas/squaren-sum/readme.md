<!-- -*- coding: windows-1252 -*- -->
# [Square(n) Sum](https://www.codewars.com/kata/515e271a311df0350d00000f)   
#### *Kyu*: **8**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/515e271a311df0350d00000f>