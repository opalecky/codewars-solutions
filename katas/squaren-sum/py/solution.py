def square_sum(numbers):
    return sum([n**2 for n in numbers])
    #your code here