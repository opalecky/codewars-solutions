<!-- -*- coding: windows-1252 -*- -->
# [Sum of Intervals](https://www.codewars.com/kata/52b7ed099cdc285c300001cd)   
#### *Kyu*: **4**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/52b7ed099cdc285c300001cd>