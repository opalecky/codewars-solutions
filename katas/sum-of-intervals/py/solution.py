def intersetion(int1, int2):
    right_intersection = int2[0] <= int1[1] <= int2[1]
    left_intersection = int1[0] <= int2[1] <= int1[1]
    int1_contains_int2 = int1[0] <= int2[0] <= int1[1] and int1[0] <= int2[1] <= int1[1]
    int2_contains_int1 = int2[0] <= int1[0] <= int2[1] and int2[0] <= int1[1] <= int2[1]
    is_within = int1_contains_int2 or int2_contains_int1
    return left_intersection or right_intersection or is_within


def union(int1, int2):
    otp = min([int1[0], int2[0]]), max([int1[1], int2[1]])
    return otp


def get_length(interval):
    return interval[1] - interval[0]


def has_intersections(intervals):
    for interval in intervals:
        for i in intervals:
            if i is interval:
                continue
            if intersetion(interval, i):
                return True
    return False


def sum_of_intervals(intervals):
    intervals = set(intervals)
    intervals = list(intervals)
    while has_intersections(intervals):
        ints = []
        for interval in intervals:
            inter = False
            for i, act in enumerate(ints):
                if intersetion(act, interval):
                    ints[i] = union(interval, act)
                    inter = True
            if not inter:
                ints.append(interval)
        intervals = set(ints)
        intervals = list(intervals)
        
    return sum([get_length(x) for x in intervals])
