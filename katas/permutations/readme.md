<!-- -*- coding: windows-1252 -*- -->
# [Permutations](https://www.codewars.com/kata/5254ca2719453dcc0b00027d)   
#### *Kyu*: **4**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/5254ca2719453dcc0b00027d>