def move_first_char(string):
    output = set()
    for iterator in range(len(string)+1):
        if not iterator:
            continue
        output.add(''.join([string[1:iterator], string[0], string[iterator:]]))
    return list(output)



def permutations(string):
    otp = {string}
    depth = 0
    while depth < len(string):
        for element in list(otp):
            for options in move_first_char(element):
                otp.add(options)
        depth += 1
    return list(otp)
    
    #your code here