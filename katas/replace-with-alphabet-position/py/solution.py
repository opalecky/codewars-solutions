alphabet = list("abcdefghijklmnopqrstuvwxyz")


def alphabet_position(text):
    otp = []
    split_input = text.split()
    for word in split_input:
        list_of_characters = list(word)
        for char in list_of_characters:
            ind = alphabet.index(char.lower()) if char.lower() in alphabet else -1
            if ind != -1:
                otp.append(str(ind+1))
                           

    return ' '.join(otp)
