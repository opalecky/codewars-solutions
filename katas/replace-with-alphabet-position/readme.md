<!-- -*- coding: windows-1252 -*- -->
# [Replace With Alphabet Position](https://www.codewars.com/kata/546f922b54af40e1e90001da)   
#### *Kyu*: **6**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/546f922b54af40e1e90001da>