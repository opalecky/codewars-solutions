def namelist(names):
    if not len(names):
        return ''
    if len(names) == 1:
        return names[0]['name']
    last = names[len(names)-1]
    names.pop(len(names)-1)
    main = []
    for character in names:
        main.append(character['name'])
    return ', '.join(main) + ' & ' + last['name']
    