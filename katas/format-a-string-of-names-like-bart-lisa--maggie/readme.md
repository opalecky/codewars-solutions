<!-- -*- coding: windows-1252 -*- -->
# [Format a string of names like 'Bart, Lisa & Maggie'.](https://www.codewars.com/kata/53368a47e38700bd8300030d)   
#### *Kyu*: **-1**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/53368a47e38700bd8300030d>