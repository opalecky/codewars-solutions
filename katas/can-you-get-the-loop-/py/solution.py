def loop_size(node):
    traversal = node
    tail = []
    cycle = []
    while id(traversal) not in tail or not len(tail):
        tail.append(id(traversal))
        traversal = traversal.next
    
    i = tail.index(id(traversal))
    cycle = tail[i:]
    tail = tail[:i]
    return len(cycle)