<!-- -*- coding: windows-1252 -*- -->
# [Can you get the loop ?](https://www.codewars.com/kata/52a89c2ea8ddc5547a000863)   
#### *Kyu*: **5**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/52a89c2ea8ddc5547a000863>