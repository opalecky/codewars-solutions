def spin_words(sentence):
    return " ".join(["".join(reversed(list(w))) if len(w) > 4 else w for w in sentence.split(" ")])