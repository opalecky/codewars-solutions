def rgb(r, g, b):
    
    return '{rr}{gg}{bb}'.format(
        rr=(str('0'*int(2-len(hex(r).replace('0x', '')))+hex(r).replace('0x', '')) if 0 <= r <= 255 else '00' if r < 255 else 'FF').upper(),
        gg=(str('0'*int(2-len(hex(g).replace('0x', '')))+hex(g).replace('0x', '')) if 0 <= g <= 255 else '00'if g < 255 else 'FF').upper(),
        bb=(str('0'*int(2-len(hex(b).replace('0x', '')))+hex(b).replace('0x', '')) if 0 <= b <= 255 else '00'if b < 255 else 'FF').upper()
    )