<!-- -*- coding: windows-1252 -*- -->
# [RGB To Hex Conversion](https://www.codewars.com/kata/513e08acc600c94f01000001)   
#### *Kyu*: **5**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/513e08acc600c94f01000001>