<!-- -*- coding: windows-1252 -*- -->
# [Array.diff](https://www.codewars.com/kata/523f5d21c841566fde000009)   
#### *Kyu*: **6**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/523f5d21c841566fde000009>