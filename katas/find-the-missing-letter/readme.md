<!-- -*- coding: windows-1252 -*- -->
# [Find the missing letter](https://www.codewars.com/kata/5839edaa6754d6fec10000a2)   
#### *Kyu*: **6**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/5839edaa6754d6fec10000a2>