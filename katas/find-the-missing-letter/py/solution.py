def find_missing_letter(chars):
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    if(chars[0].isupper()):
        alphabet = alphabet.upper()
        
    begin_index = alphabet.index(chars[0])
    sub = alphabet[begin_index:begin_index+len(chars)+1]
    for i in list(sub):
        if not i in chars:
            return i
        
    return ''

