<!-- -*- coding: windows-1252 -*- -->
# [Isograms](https://www.codewars.com/kata/54ba84be607a92aa900000f1)   
#### *Kyu*: **7**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/54ba84be607a92aa900000f1>