
#include <set>

bool is_isogram(std::string str) {
  std::set<char> used;
    for(auto character : str)
        if(used.count(tolower(character)) > 0)
            return false;
        else
            used.insert(tolower(character));
    return true;
}