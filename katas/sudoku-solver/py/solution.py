from math import floor


# todo: blocked values. If it is obvious that field has to go in a certain block in a row or column it is blocked for other blocks requiring that value in row/column
class SudokuField:
    def __init__(self, board, position=(0, 0), value=0):
        self.position = position
        self.value = value
        self.missing = set()
        self.board = board
        self.locked = False
        self.selected = False

        self.block = None

    def select(self):
        self.selected = True

    def deselect(self):
        self.selected = False

    def lock(self, state):
        self.locked = state
        return self

    def set_value(self, new_value=None, called_from_block=False, called_from_board=False):
        if new_value is not None and new_value != 0 and not self.locked and new_value in self.missing:
            self.value = new_value
            self.missing.remove(new_value)
            self.lock(True)
            if not called_from_block:
                self.board.add_block_value(self.position, new_value, True)

    def clear_value(self):
        self.value = 0

    def add_missing(self, value):
        if value == self.value:
            return
        self.missing.add(value)

    def find_obvious_fil_ins(self):
        if len(self.missing) == 1:
            return next(iter(self.missing))

        # todo: option reduction
        #       if multiple fields in the same row are empty check if they also need
        # todo: if blocks in rows or columns around have given value the only obvious place is in the tested row/column


class SudokuBlock:
    def __init__(self, board, col, row):
        self.row = row
        self.column = col
        self.values = set()
        self.fields = set()
        self.board = board
        for i in range(3):
            for j in range(3):
                self.fields.add(f'{3 * col + i}x{3 * row + j}')
                self.board.fields[f'{3 * col + i}x{3 * row + j}'].block = self

    def add_value(self, value, position=(0, 0), called_from_field=False):
        x = 3 * self.column + position[0] % 3
        y = 3 * self.row + position[1] % 3
        self.values.add(value)
        key = f'{x}x{y}'
        if not called_from_field:
            self.board.fields[key].set_value(value, True, True)

    def contains_value(self, value):
        return value in self.values

    def is_done(self):
        return len(self.values) == 9


class Sudoku:
    def __init__(self, arr):
        self.fields = {}
        self.size = len(arr)
        self.blocks = set()
        self.running = True

        self.selected_key = "0x0"

        for i in range(self.size ** 2):
            c = i % self.size
            r = floor(i / self.size)
            f = SudokuField(self, (c, r), arr[r][c]).lock(arr[r][c] != 0)
            self.fields[f"{c}x{r}"] = f

        for i in range(9):
            self.blocks.add(SudokuBlock(self, i % 3, floor(i / 3)))

        self.select_field("0x0")

    def keypress(self, e):
        if e.char == "w":
            self.move_up()
        if e.char == "s":
            self.move_down()
        if e.char == "a":
            self.move_left()
        if e.char == "d":
            self.move_right()
        if e.char.isdigit():
            self.fields[self.selected_key].set_value(int(e.char))
            self.find_missing()

    def select_field(self, key):
        self.fields[self.selected_key].deselect()
        self.selected_key = key
        self.fields[self.selected_key].select()

    def move_up(self):
        self.select_field(f"{self.selected_key[0]}x{abs(int(self.selected_key[2]) - 1)}")

    def move_down(self):
        self.select_field(f"{self.selected_key[0]}x{abs(int(self.selected_key[2]) + 1)}")

    def move_left(self):
        self.select_field(f"{abs(int(self.selected_key[0]) - 1)}x{self.selected_key[2]}")

    def move_right(self):
        self.select_field(f"{abs(int(self.selected_key[0]) + 1)}x{self.selected_key[2]}")

    def add_block_value(self, position, value, add_from_field=False):
        pos = (floor(position[0] / 3), floor(position[1] / 3))
        for b in self.blocks:
            if (b.column, b.row) == pos:
                b.add_value(value, pos, add_from_field)
                break

    def print(self):
        line = ""
        for f in self.fields.keys():
            field = self.fields[f]
            if field.selected:
                line += (f" \033[31;1;4m{str(field.value)}\033[0m ")
            else:
                line += (f" {str(field.value)} ")

            if f[0] == "8":
                print(f'{line}')
                line = ""

    def find_missing(self):
        red_fields = []
        vals = set()
        for field in self.fields.values():
            if field.locked:
                continue
            missing_vals = field.missing if len(field.missing) > 0 else range(1, 10, 1)
            field.missing = set()
            for f in self.fields.keys():
                add = False
                tf = self.fields[f]
                if f[2] == str(field.position[1]):
                    red_fields.append(tf)
                    add = True
                elif f[0] == str(field.position[0]):
                    red_fields.append(tf)
                    add = True
                elif tf.block is field.block:
                    red_fields.append(tf)
                    add = True
                if add:
                    vals.add(tf.value)

            for i in missing_vals:
                if i in vals or i == field.value:
                    continue
                field.add_missing(i)
            red_fields = []
            vals = set()

    def output(self):
        otp = [[] for _ in range(9)]
        for field in self.fields.values():
            otp[field.position[1]].insert(field.position[0], field.value)
        return otp

    def is_solved(self):
        _sum = sum([f.value for f in self.fields.values()])
        return _sum == 45*9

    def solve(self):
        import time
        start = time.time()
        self.find_missing()
        while not self.is_solved() and (time.time() - start) < 5:
            found_obvious = []

            for field in self.fields.values():
                val = field.find_obvious_fil_ins()
                if val:
                    found_obvious.append(val)
                field.set_value(val, called_from_board=True)
            if self.is_solved():
                return self.output()
            elif not len(found_obvious):
                return []
            self.find_missing()
        return self.output()

def sudoku(puzzle):
    return Sudoku(puzzle).solve()