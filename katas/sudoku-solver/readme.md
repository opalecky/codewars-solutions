<!-- -*- coding: windows-1252 -*- -->
# [Sudoku Solver](https://www.codewars.com/kata/5296bc77afba8baa690002d7)   
#### *Kyu*: **3**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/5296bc77afba8baa690002d7>