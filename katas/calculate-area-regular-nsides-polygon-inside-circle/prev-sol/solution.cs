﻿namespace DefaultNamespace;

public class solution
{
    public static double AreaOfPolygonInsideCircle(double circleRadius, int numberOfSides)
    {
        if (circleRadius < 0) throw new ArgumentException("Argument circleRadius is negative!");
        if (numberOfSides < 3) throw new ArgumentException("Less than 3 sides are not suffient for polygon creation!");
        
        double alpha =  (2 * Math.PI) / numberOfSides;
        double area = numberOfSides * (1 / 2 * (numberOfSides * circleRadius * alpha) *
                                       Math.sqrt(Math.pow(circleRadius, 2) - Math.pow(circleRadius * alpha) / 2, 2));
        return area;
    }
}