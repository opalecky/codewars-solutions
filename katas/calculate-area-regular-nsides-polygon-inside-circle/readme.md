# [Calculate the area of a regular n sides polygon inside a circle of radius r](https://www.codewars.com/kata/5a58ca28e626c55ae000018a)
#### *Kyu*: 6  
> Should calculate the area of a regular polygon of numberOfSides, number-of-sides, or number_of_sides sides inside a circle of radius circleRadius, circle-radius, or circle_radius which passes through all the vertices of the polygon (such circle is called circumscribed circle or circumcircle). The answer should be a number rounded to 3 decimal places.
## Input :: Output Examples
```
AreaOfPolygonInsideCircle(3, 3) // returns 11.691

AreaOfPolygonInsideCircle(5.8, 7) // returns 92.053

AreaOfPolygonInsideCircle(4, 5) // returns 38.042
```

## Solution Description  
![Solition chart](./flowchart.jpg "Flowchart")  

### Pseudo-code  
```
double Area(double r, int n) {
    /**
      * @param r - Radius
      * @param n - Amount of sides
      *
      * Calculate area of inscribed n-sided polygon within a circle of radius R
      */

    if r < 0 -> ParameterException
    if n < 3 -> ParameterException
    double area = n*(((r*sin(2*pi)/n)/2)) * sqrt((r^2)-(((r * sin( 2 * pi) / n) / 2) ^ 2)))
    
    return area
}
```

Note: if you need to use Pi in your code, use the native value of your language unless stated otherwise.

Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/5a58ca28e626c55ae000018a>
