﻿using NUnit.Framework;

namespace CARNPIC
{
    public class Tests
    {
        [Test]
        public void UnitTest()
        {
            Assert.AreEqual(11.691, Solution.AreaOfPolygonInsideCircle(3, 3));
            Assert.AreEqual(8, Solution.AreaOfPolygonInsideCircle(2, 4));
            Assert.AreEqual(14.86, Solution.AreaOfPolygonInsideCircle(2.5, 5));
        }
    }
}