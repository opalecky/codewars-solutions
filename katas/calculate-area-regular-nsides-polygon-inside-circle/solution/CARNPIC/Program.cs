﻿namespace CARNPIC
{
    public class Solution
    {
        public static double AreaOfPolygonInsideCircle(double circleRadius, int numberOfSides)
        {
            if (circleRadius < 0) throw new ArgumentException("Argument circleRadius is negative!");
            if (numberOfSides < 3) throw new ArgumentException("Less than 3 sides are not sufficient for polygon creation!");
            return Math.Round(numberOfSides * ((circleRadius * Math.Sin((2 * Math.PI) / numberOfSides / 2)) * Math.Sqrt(Math.Pow(circleRadius, 2) - Math.Pow((circleRadius * Math.Sin((2 * Math.PI) / numberOfSides / 2)), 2))), 3);
        }
    }
}