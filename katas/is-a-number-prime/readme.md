<!-- -*- coding: windows-1252 -*- -->
# [Is a number prime?](https://www.codewars.com/kata/5262119038c0985a5b00029f)   
#### *Kyu*: **6**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/5262119038c0985a5b00029f>