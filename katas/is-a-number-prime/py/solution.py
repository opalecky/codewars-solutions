def is_prime(num):
    if num < 2:
        return False
    if num == 2:
        return True
    if num <= 5:
        return num % 2
    if not num % 2 or not num % 3 or not num % 5:
        return False
    i = 7;
    while i ** 2 <= num:
        if num % (i+2) == 0 or num % i == 0:
            return False
        i += 4
    return True