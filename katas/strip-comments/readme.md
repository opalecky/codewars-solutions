<!-- -*- coding: windows-1252 -*- -->
# [Strip Comments](https://www.codewars.com/kata/51c8e37cee245da6b40000bd)   
#### *Kyu*: **4**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/51c8e37cee245da6b40000bd>