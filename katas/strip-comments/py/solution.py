def find_first_marker(string, markers):
    otp = [markers[0], len(string)]
    for marker in markers:
        if marker in string:
            ind = string.index(marker)
            if ind < otp[1]:
                otp = [marker, ind]
    return otp
def solution(string,markers):
    print(string, '\n')
    otp = []
    
    for s in string.split('\n'):
        first_marker = find_first_marker(s, markers) if markers else None
        print(first_marker)
        if not first_marker:
            otp.append(s)
            continue
            
        st = s.strip(' ')
        if first_marker[0] in s:
            i = first_marker[1]
            st = s[:i]
            for m in markers:
                st.replace(m, '')
                print(st)
                
        otp.append(st.strip(' '))        
    otp = '\n'.join(otp)
    print(otp)
    return otp