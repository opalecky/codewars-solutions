<!-- -*- coding: windows-1252 -*- -->
# [Rectangle into Squares](https://www.codewars.com/kata/55466989aeecab5aac00003e)   
#### *Kyu*: **6**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/55466989aeecab5aac00003e>