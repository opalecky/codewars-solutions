#include <vector>

class SqInRect
{
  public:
  static std::vector<int> sqInRect(int lng, int wdth);
};
std::vector< int > SqInRect::sqInRect( int lng, int wdth ) {
    if ( lng == wdth ) {
        return {};
    }

    int total_size = lng * wdth;

    std::vector< int > otp;

    while ( total_size ) {
        if ( lng >= wdth ) {
            otp.push_back( wdth );
            lng -= wdth;
        } else {
            otp.push_back( lng );
            wdth -= lng;
        }

        total_size = lng * wdth;
    }
    return otp;
};