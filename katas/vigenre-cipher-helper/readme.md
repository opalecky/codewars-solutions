<!-- -*- coding: windows-1252 -*- -->
# [Vigenère Cipher Helper](https://www.codewars.com/kata/52d1bd3694d26f8d6e0000d3)   
#### *Kyu*: **4**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/52d1bd3694d26f8d6e0000d3>