class VigenereCipher:
    def __init__(self, key, alphabet):
        self.base_key = key
        self.key = key
        self.alphabet = alphabet

    def __run_cipher(self, string, reverse=False):
        otp = ''
        self.key = self.__extend_key(self.base_key, len(string))
        for i, letter in enumerate(string):
            shift = self.alphabet.index(self.key[i])
            if reverse:
                shift *= -1
            if letter in self.alphabet:
                otp += self.alphabet[(self.alphabet.index(letter)+shift) % len(self.alphabet)]
            else:
                otp += letter
        return otp

    def encode(self, string):
        return self.__run_cipher(string)

    def decode(self, string):
        return self.__run_cipher(string, True)

    @staticmethod
    def __extend_key(key_inner, length):
        otp = ''
        for i in range(length):
            otp += key_inner[i % len(key_inner)]
        return otp
