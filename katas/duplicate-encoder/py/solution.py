def char_counter(word):
    otp = {}
    for char in list(word):
        l = char.lower()
        
        if not l in otp:
            otp[l] = 1
        else:
            otp[l] += 1
            
    return otp
def duplicate_encode(word):
    cc = char_counter(word)
    print(cc)
    otp = ''
    for char in list(word):
        otp += '(' if cc[char.lower()] == 1 else ')'
    return otp