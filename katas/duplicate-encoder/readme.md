<!-- -*- coding: windows-1252 -*- -->
# [Duplicate Encoder](https://www.codewars.com/kata/54b42f9314d9229fd6000d9c)   
#### *Kyu*: **6**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/54b42f9314d9229fd6000d9c>