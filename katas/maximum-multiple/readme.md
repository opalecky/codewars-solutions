<!-- -*- coding: windows-1252 -*- -->
# [Maximum Multiple](https://www.codewars.com/kata/5aba780a6a176b029800041c)   
#### *Kyu*: **7**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/5aba780a6a176b029800041c>