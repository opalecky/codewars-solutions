<!-- -*- coding: windows-1252 -*- -->
# [Valid Parentheses](https://www.codewars.com/kata/52774a314c2333f0a7000688)   
#### *Kyu*: **5**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/52774a314c2333f0a7000688>