def valid_parentheses(string):
    ctx = {'(':')', '[':']', '{':'}'}
    stack = []
     
    for char in string:
        if char in ctx.keys():
            stack.append(char)
        elif char in ctx.values():
            if len(stack) and char == ctx[stack[-1]]:
                stack.pop()
            else:
                return False
        else:
            continue
            
    return len(stack) == 0
            
           