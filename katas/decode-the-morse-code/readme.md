<!-- -*- coding: windows-1252 -*- -->
# [Decode the Morse code](https://www.codewars.com/kata/54b724efac3d5402db00065e)   
#### *Kyu*: **6**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/54b724efac3d5402db00065e>