public class MorseCodeDecoder {
    public static String decode(String morseCode) {
      String [] words = morseCode.split("  ");
      StringBuilder out_String = new StringBuilder();
      for (String word : words) {
        String [] w = word.split(" ");
        for (String ch:w) {
          String tr = MorseCode.get(ch);
          if(tr != null) {
            out_String.append(tr);
          }
        }
        out_String.append(" ");
      }
      return out_String.toString().trim();
    }
}