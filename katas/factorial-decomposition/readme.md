<!-- -*- coding: windows-1252 -*- -->
# [Factorial decomposition](https://www.codewars.com/kata/5a045fee46d843effa000070)   
#### *Kyu*: **5**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/5a045fee46d843effa000070>