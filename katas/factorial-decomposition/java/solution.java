import java.util.ArrayList;
import java.util.Collections;
class FactDecomp {
    
        public static String decomp(int n) {
        ArrayList<Integer> primes = new ArrayList<>();
        primes.add(2);
        for (int p = 3; p <= n; p++) {
            if (p % 2 == 0) continue;
            boolean is = true;
            for (int l = 2; l < p/2; l++) {
                if (p%l == 0) {
                    is = false;
                    break;
                }
            }
            if(is) {
                primes.add(p);
            }
        }
        ArrayList<Integer> decomp = new ArrayList<>();
        while(n > 1){
            int sub = n;
            int current_prime = 0;
            while (sub != 1) {
                int p = primes.get(current_prime);
                if (sub % p != 0) current_prime++;
                else {
                    decomp.add(p);
                    sub /= p;
                }
            }
            n--;
        }
        Collections.sort(decomp);
        StringBuilder otp = new StringBuilder();
       while (decomp.size() > 0) {
            int val = decomp.get(0);
            int size = decomp.subList(0, decomp.lastIndexOf(val)+1).size();

            otp.append(val);
            if(size > 1) otp.append("^"+size);
            otp.append(" * ");

            decomp.removeIf(i -> i==val);
        }
        return otp.substring(0, otp.length()-3);
    }

} 
