<!-- -*- coding: windows-1252 -*- -->
# [Sum of Digits / Digital Root](https://www.codewars.com/kata/541c8630095125aba6000c00)   
#### *Kyu*: **6**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/541c8630095125aba6000c00>