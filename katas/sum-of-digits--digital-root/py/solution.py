def digital_root(n):
    while len(str(n)) > 1:
        dig = list(str(n))
        n = sum([int(d) for d in dig])
    return n
    # ...