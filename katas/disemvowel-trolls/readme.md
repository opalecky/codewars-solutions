<!-- -*- coding: windows-1252 -*- -->
# [Disemvowel Trolls](https://www.codewars.com/kata/52fba66badcd10859f00097e)   
#### *Kyu*: **7**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/52fba66badcd10859f00097e>