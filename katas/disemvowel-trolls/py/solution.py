vowels = list("aeiou")
def disemvowel(string_):
    otp = ''
    for char in list(string_):
        if not char.lower() in vowels:
            otp += char
            
    string_ = otp
    return string_