<!-- -*- coding: windows-1252 -*- -->
# [Sum of odd numbers](https://www.codewars.com/kata/55fd2d567d94ac3bc9000064)   
#### *Kyu*: **7**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/55fd2d567d94ac3bc9000064>