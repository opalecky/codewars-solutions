long long rowSumOddNumbers( unsigned n ) {
    float temp = n;
    float first_in_line = temp * ( temp - 1 ) + 1;
    float last_in_line = first_in_line + 2 * (temp-1);
    float otp = ( temp / 2 ) * ( first_in_line + last_in_line );
    return (long long int) otp;
}