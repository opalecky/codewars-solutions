def fibonacci():
    n=0
    otp = []
    while 1:
        if n < 2: otp.append(n); yield n  
        else: otp.append(otp[n-2]+otp[n-1]); yield otp[n]
        n += 1

def productFib(prod):
    otp = [0,0,0]
    fib = fibonacci()
    while otp[2] < prod:
        otp[0] = next(fib) if otp[1] == 0 else otp[1]
        otp[1] = next(fib)
        otp[2] = otp[0] * otp[1]
    return [otp[0], otp[1] or 1, otp[2] == prod] 