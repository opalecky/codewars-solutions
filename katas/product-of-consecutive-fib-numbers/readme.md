<!-- -*- coding: windows-1252 -*- -->
# [Product of consecutive Fib numbers](https://www.codewars.com/kata/5541f58a944b85ce6d00006a)   
#### *Kyu*: **5**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/5541f58a944b85ce6d00006a>