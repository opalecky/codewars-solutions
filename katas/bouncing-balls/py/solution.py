def bouncing_ball(h, bounce, window):
    # get rid of parameter validation
    if h <= 0 or bounce <= 0 or bounce >= 1 or window >= h:
        return -1
    
    # initial bounce only makes mum see it once
    bounce_h = h * bounce
    bounces = 1

    # physics baby
    while bounce_h > window:
        bounces += 2
        bounce_h *= bounce
    
    return bounces