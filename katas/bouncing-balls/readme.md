<!-- -*- coding: windows-1252 -*- -->
# [Bouncing Balls](https://www.codewars.com/kata/5544c7a5cb454edb3c000047)   
#### *Kyu*: **6**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/5544c7a5cb454edb3c000047>