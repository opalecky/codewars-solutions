#include <string>

std::string createPhoneNumber(const int arr [10]){
  std::string first_group, second_group, third_group;
  for(int i=0;i<10;i++){
    if(i<3)first_group += std::to_string(arr[i]);
    else if(i<6)second_group += std::to_string(arr[i]);
    else third_group += std::to_string(arr[i]);
  }
  std::string output = "("+first_group+") "+second_group+"-"+third_group;
    return output;
}