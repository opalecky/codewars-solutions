<!-- -*- coding: windows-1252 -*- -->
# [Directions Reduction](https://www.codewars.com/kata/550f22f4d758534c1100025a)   
#### *Kyu*: **5**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/550f22f4d758534c1100025a>