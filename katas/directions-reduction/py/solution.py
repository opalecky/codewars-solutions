OPPOSITES = {
    "NORTH": 'SOUTH',
    "WEST": 'EAST',
    "EAST": 'WEST',
    "SOUTH": 'NORTH',
}


def dirReduc(arr):
    test = True
    while test:
        save = []
        test = False
        skip = False
        for i, dir in enumerate(arr):
            if skip:
                skip = False
                continue
            if i+1 != len(arr) and dir == OPPOSITES[arr[i+1]]:
                skip = True
                test = True
            else:
                save.append(dir)
        arr = save
    return arr
