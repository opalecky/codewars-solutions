
typedef unsigned int Sudoku[9][9];
template < typename T >
bool vector_contains( std::vector< T >& v, T* val ) {
    bool contains = false;
    for ( T i : v ) {
        if ( i == *val ) {
            contains = true;
        }
    }
    return contains;
}

template < typename T >
void vector_remove( std::vector< T >& v, const T& val ) {
    for ( size_t i = 0; i != v.size(); i ++ ) {
        if ( v[ i ] == val ) {
            v[ i ] = v[ v.size()-1 ];
            v.pop_back();
            return;
        }
    }
}

bool is_row_valid( unsigned int row[9] ) {
    std::vector< unsigned int > used{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    for ( size_t i = 0; i < 9; i ++ ) {
        unsigned int value = row[ i ];
        if ( ! vector_contains( used, &value ) ) return false;
        vector_remove( used, value );
    }
    return true;
}


bool is_block_valid( unsigned int block[3][3] ) {
    std::vector< unsigned int > used{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    for ( size_t i = 0; i < 3; i ++ )
        for ( size_t j = 0; j < 3; j ++ ) {
            if ( ! vector_contains( used, &block[ i ][ j ] ) ) return false;
            vector_remove( used, block[ i ][ j ] );

        }
    return true;
}


bool validSolution( Sudoku s_case ) {
    int valid[3] = { 0 };
    int row = 0;
    int col = 0;
    for ( int i = 0; i < 9; i ++ ) {
        if ( is_row_valid( s_case[ i ] ) ) valid[ 0 ] ++;

        unsigned int temp[9]{ s_case[ 0 ][ i ], s_case[ 1 ][ i ], s_case[ 2 ][ i ], s_case[ 3 ][ i ], s_case[ 4 ][ i ],
                     s_case[ 5 ][ i ], s_case[ 6 ][ i ], s_case[ 7 ][ i ], s_case[ 8 ][ i ] };
        if ( is_row_valid( temp ) ) valid[ 1 ] ++;

        row = i % 3;
        if ( row == 0 && i != 0 ) col ++;
        unsigned int block[3][3]
        {
            { s_case[ row * 3 ][ col * 3 ],s_case[ row * 3 ][ col * 3 + 1 ],s_case[ row * 3 ][ col * 3 +2 ] },
            { s_case[ row * 3 + 1 ][ col * 3 ], s_case[ row * 3 + 1 ][ col * 3 + 1 ], s_case[ row * 3 + 1 ][col * 3 + 2 ] },
            { s_case[ row * 3 + 2 ][ col * 3 ], s_case[ row * 3 + 2 ][ col * 3 + 1 ], s_case[ row * 3 + 2 ][col * 3 + 2 ] }
        };

        if ( is_block_valid( block ) ) valid[ 2 ] ++;
    }
    return valid[ 0 ] == 9 && valid[ 1 ] == 9 && valid[ 2 ] == 9;
}

std::string print_boolean( bool b ) {
    return b ? "True" : "False";
}