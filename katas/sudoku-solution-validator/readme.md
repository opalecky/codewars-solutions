<!-- -*- coding: windows-1252 -*- -->
# [Sudoku Solution Validator](https://www.codewars.com/kata/529bf0e9bdf7657179000008)   
#### *Kyu*: **4**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/529bf0e9bdf7657179000008>