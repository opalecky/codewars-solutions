<!-- -*- coding: windows-1252 -*- -->
# [Two Oldest Ages](https://www.codewars.com/kata/511f11d355fe575d2c000001)   
#### *Kyu*: **7**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/511f11d355fe575d2c000001>