package solution

fun twoOldestAges(ages: List<Int>): List<Int> {
    val oldest = ages.sorted().reversed();
    return listOf(oldest[1],oldest[0]);
}