list = {
'A':'Alfa',
'B':'Bravo',
'C':'Charlie',
'D':'Delta',
'E':'Echo',
'F':'Foxtrot',
'G':'Golf',
'H':'Hotel',
'I':'India',
'J':'Juliett',
'K':'Kilo',
'L':'Lima',
'M':'Mike',
'N':'November',
'O':'Oscar',
'P':'Papa',
'Q':'Quebec',
'R':'Romeo',
'S':'Sierra',
'T':'Tango',
'U':'Uniform',
'V':'Victor',
'W':'Whiskey',
'X':'Xray',
'Y':'Yankee',
'Z':'Zulu',
'?':'?',
'.':'.',
'_':'_',
'-':'-',
'!':'!',
'<':'<',
'>':'>'
}
def to_nato(words):
    otp = ''
    for i, word in enumerate(words):
        w = word.upper()
        if w not in list.keys():
            continue
        otp += list[w]
        if i != len(words)-1:
            otp+= ' '
    return otp