<!-- -*- coding: windows-1252 -*- -->
# [Quarter of the year](https://www.codewars.com/kata/5ce9c1000bab0b001134f5af)   
#### *Kyu*: **8**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/5ce9c1000bab0b001134f5af>