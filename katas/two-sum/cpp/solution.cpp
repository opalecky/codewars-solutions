std::pair<std::size_t, std::size_t> two_sum(const std::vector<int>& numbers, int target) {
    
    for(int i = 0; i < numbers.size(); i++) {
      int remainder = target - numbers[i];
      for(int j = 0; j < numbers.size(); j++) {
//         std::cout << numbers[j] << " = " << remainder <<std::endl;
        if(numbers[j] == remainder && i!=j) {
          return {i, j};
        } 
      }
    }
  
    return {0,0};
}