<!-- -*- coding: windows-1252 -*- -->
# [Two Sum](https://www.codewars.com/kata/52c31f8e6605bcc646000082)   
#### *Kyu*: **6**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/52c31f8e6605bcc646000082>