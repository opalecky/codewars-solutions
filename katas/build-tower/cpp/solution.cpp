class Kata
{
public:
    std::vector<std::string> towerBuilder(int nFloors)
    {
      std::vector< std::string > otp;
      int last_length = 2 * nFloors - 1;
      auto repeatString = [ ](const char* s, int count ) {
          std::string out;
          for(int i = 0; i<count; i++) {
              out += s;
          }
          return out;
      };
      for ( int i = 1; i <= nFloors; i ++ ) {
          int tower_width = 1+((i-1)*2);
          std::string padding = repeatString(" ", (last_length-tower_width)/2);

          otp.push_back(padding.append(repeatString("*", tower_width)+padding));
      }
      return otp;

    }
};