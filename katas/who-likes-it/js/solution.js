function likes(names) {
  if(names.length != 0) {
    if(names.length == 1) {
      return names[0]+" likes this"
    } else if(names.length <= 3) {
      var main_string = "";
      for(a=0; a < names.length; a++) {
        main_string += names[a];
        if(a < names.length-1 ) {
          main_string += (names.length-a == 2 ? " and " : ", ");
        }
      }
      return main_string+" like this";
    } else if( names.length >= 4 ) {
      return names[0] + ", "+names[1] + " and "+(names.length-2)+" others like this";
    }
  } else {
    return "no one likes this";
  }
}