<!-- -*- coding: windows-1252 -*- -->
# [Descending Order](https://www.codewars.com/kata/5467e4d82edf8bbf40000155)   
#### *Kyu*: **7**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/5467e4d82edf8bbf40000155>