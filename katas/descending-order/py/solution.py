def descending_order(num):
    arr = list(str(num))
    arr.sort(reverse=True)
    return int(''.join(arr))
