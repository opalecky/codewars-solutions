export class G964 {
  public static inArray(a1: string[], a2: string[]): string[] {
      return a1.filter(s => !!a2.filter(s1 => s1.includes(s)).length).sort()
  }
}