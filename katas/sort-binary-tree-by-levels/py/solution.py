def append_values(node, otp):
    if node.left:
        otp.append(node.left.value)
    if node.right:
        otp.append(node.right.value)


def tree_by_levels(node):
    if node is None:
        return []
    otp = [node.value]
    queue = [node]
    while len(queue):
        append_values(queue[0], otp)
        if queue[0].left:
            queue.append(queue[0].left)
        if queue[0].right:
            queue.append(queue[0].right)
        queue = queue[1:]

    return otp
