<!-- -*- coding: windows-1252 -*- -->
# [Sort binary tree by levels](https://www.codewars.com/kata/52bef5e3588c56132c0003bc)   
#### *Kyu*: **4**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/52bef5e3588c56132c0003bc>