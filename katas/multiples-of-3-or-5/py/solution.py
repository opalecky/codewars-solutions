def solution(number):
    threes = [i for i in range(0,number,3)]
    threes.extend([i for i in range(0,number,5)])
    s = set(threes)
    return sum(list(s))
  