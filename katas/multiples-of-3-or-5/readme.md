<!-- -*- coding: windows-1252 -*- -->
# [Multiples of 3 or 5](https://www.codewars.com/kata/514b92a657cdc65150000006)   
#### *Kyu*: **6**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/514b92a657cdc65150000006>