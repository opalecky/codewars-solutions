<!-- -*- coding: windows-1252 -*- -->
# [Merge two arrays](https://www.codewars.com/kata/583af10620dda4da270000c5)   
#### *Kyu*: **7**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/583af10620dda4da270000c5>