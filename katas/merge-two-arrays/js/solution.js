function mergeArrays(a, b) {
   const out_array = [];
   if(a.length >= b.length) {
    for(const i in a) {
      out_array.push(a[i])
      if (b.hasOwnProperty(i)){
        out_array.push(b[i])
      }
    }
   }
   else {
    for(const i in b) {
      if (a.hasOwnProperty(i)){
        out_array.push(a[i])
      }
      out_array.push(b[i])
    }
   }
  return out_array
}