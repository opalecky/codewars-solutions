<!-- -*- coding: windows-1252 -*- -->
# [What's a Perfect Power anyway?](https://www.codewars.com/kata/54d4c8b08776e4ad92000835)   
#### *Kyu*: **5**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/54d4c8b08776e4ad92000835>