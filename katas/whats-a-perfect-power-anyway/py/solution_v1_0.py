import math
def isPP(n):
    r = 2
    while r <= math.sqrt(n)+1:
        t = round(math.log(n,r),5)
        if t%1 == 0 and r**t == n:
            return [r, int(t)]
        r+=1
    return None