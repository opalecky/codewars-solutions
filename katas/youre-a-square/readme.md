<!-- -*- coding: windows-1252 -*- -->
# [You're a square!](https://www.codewars.com/kata/54c27a33fb7da0db0100040e)   
#### *Kyu*: **7**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/54c27a33fb7da0db0100040e>