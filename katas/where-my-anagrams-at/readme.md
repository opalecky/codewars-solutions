<!-- -*- coding: windows-1252 -*- -->
# [Where my anagrams at?](https://www.codewars.com/kata/523a86aa4230ebb5420001e1)   
#### *Kyu*: **-1**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/523a86aa4230ebb5420001e1>