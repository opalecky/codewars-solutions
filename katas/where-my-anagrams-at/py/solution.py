def get_letter_count(word):
    otp = {}
    for letter in list(word):
        if not letter in otp:
            otp[letter] = 1
        else: 
            otp[letter] += 1
    return otp


def anagrams(word, words):
    anagrams_ = []
    for _word in words:
        if get_letter_count(_word) == get_letter_count(word):
            anagrams_.append(_word)
            
    return anagrams_