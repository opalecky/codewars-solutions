#include <string>

std::string repeat_str(size_t repeat, const std::string& str) {
  std::string otp;
  for(int _=0; _ < repeat; _++) otp.append(str);
  return otp;
}