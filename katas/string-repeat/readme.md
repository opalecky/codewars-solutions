<!-- -*- coding: windows-1252 -*- -->
# [String repeat](https://www.codewars.com/kata/57a0e5c372292dd76d000d7e)   
#### *Kyu*: **8**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/57a0e5c372292dd76d000d7e>