<!-- -*- coding: windows-1252 -*- -->
# [Valid Braces](https://www.codewars.com/kata/5277c8a221e209d3f6000b56)   
#### *Kyu*: **6**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/5277c8a221e209d3f6000b56>