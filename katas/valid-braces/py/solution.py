opening = list("[({")
closing = list("])}")
def validBraces(string):
    scopelist = []
    for sym in list(string):
        if sym in opening:
            scopelist.append(sym)
        else:
            if len(scopelist) > 0:
                ind_ = opening.index(scopelist[len(scopelist)-1])
                scopelist.pop(len(scopelist)-1)
                
                if ind_ != closing.index(sym):
                    return False
                
            else:
                return False
                
    return len(scopelist) == 0