def dec_to_bin(num, otp = ''):
    otp += str(num % 2)
    if num >= 1:
        return dec_to_bin(num//2, otp)
    else:
        return otp
    if otp[0] == '0':
        otp = otp[1:len(otp)]
    return otp
    
def count_bits(n):   
    return list(dec_to_bin(n)).count('1')