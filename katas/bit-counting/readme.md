<!-- -*- coding: windows-1252 -*- -->
# [Bit Counting](https://www.codewars.com/kata/526571aae218b8ee490006f4)   
#### *Kyu*: **6**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/526571aae218b8ee490006f4>