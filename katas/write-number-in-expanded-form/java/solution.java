import java.util.ArrayList;
public class Kata
{
    public static String expandedForm(int num)
    {
      final String strNum = Integer.toString(num);
      ArrayList<String> out_Array = new ArrayList<>();
      
      final int c_lengthMinusOne = strNum.length()-1;
      
      for(int i = c_lengthMinusOne; i >= 0; i--) {
        char ch = strNum.charAt(i);
        if(ch == '0') continue;
        StringBuilder currentString = new StringBuilder();
        currentString.append(ch);
        final int inverse_i = c_lengthMinusOne-i;
        for (int _z = 0; _z < inverse_i; _z++) {
          currentString.append("0");
        } 
        out_Array.add(currentString.toString());
      }
      
      StringBuilder out_String = new StringBuilder();
      
      for (int i = out_Array.size()-1; i >=0; i--) {
        StringBuilder current = new StringBuilder();
        current.append(out_Array.get(i));
        if(i != 0) {
          current.append(" + ");
        }
        out_String.append(current.toString());
      }
      
      System.out.println(out_String.toString());
      
      return out_String.toString();
    }
}