<!-- -*- coding: windows-1252 -*- -->
# [Write Number in Expanded Form](https://www.codewars.com/kata/5842df8ccbd22792a4000245)   
#### *Kyu*: **6**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/5842df8ccbd22792a4000245>