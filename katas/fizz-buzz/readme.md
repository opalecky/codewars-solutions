<!-- -*- coding: windows-1252 -*- -->
# [Fizz Buzz](https://www.codewars.com/kata/5300901726d12b80e8000498)   
#### *Kyu*: **7**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/5300901726d12b80e8000498>