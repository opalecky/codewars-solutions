using System;
using System.Collections.Generic;

public class FizzBuzz
{
  public static string[] GetFizzBuzzArray(int n)
  {
    String [] out_Array = new String[n];
    for (int i = 1; i <= n; i++) {
      String current_String = "";
      if (i % 3 == 0 ) current_String += "Fizz";
      if (i % 5 == 0) current_String += "Buzz";
      if (i % 3 != 0 && i % 5 != 0 ) current_String += i.ToString();
      out_Array[i-1] = current_String;
    }
    return out_Array;
  }
}