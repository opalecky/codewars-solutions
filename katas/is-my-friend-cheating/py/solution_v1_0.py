def remov_nb(n):
    outputs = []
    sum = (n*(1+n))/2
    for a in range(2,n,1):
        b = int((sum-a)/(a+1))
        if b >= n+1 or b % 1 > 0 or a*b != sum-(a+b):
            continue
        
        outputs.append((a,b))
    
    return outputs