<!-- -*- coding: windows-1252 -*- -->
# [Validate Credit Card Number](https://www.codewars.com/kata/5418a1dd6d8216e18a0012b2)   
#### *Kyu*: **6**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/5418a1dd6d8216e18a0012b2>