#include <cstring>
#include <string>

#define LOG(x) std::cout << x << std::endl;

class Kata {
  public:
  static int char_to_int(char i) {
    return std::stoi( std::string( 1, i ) );
}

static int digital_root( int number ) {
    int output = 0;


    for ( auto i : std::to_string( number ) ) {
        output += Kata::char_to_int(i);
    }
    if ( strlen( std::to_string( output ).c_str() ) ) {
        return output;
    }
    return digital_root( output );
}

static bool validate( long long int n ) {
  LOG(n);
    std::string number = std::to_string( n );
    const size_t num_len = strlen( number.c_str() );
    int final_sum = 0;
    for ( size_t i = 0; i < num_len; i ++ ) {
        if ( ! ( ( num_len - i ) % 2 ) ) final_sum += Kata::digital_root(Kata::char_to_int(number[ i ]) * 2);
        else final_sum += Kata::char_to_int(number[ i ] );
    }
    return !((final_sum)%10);
}
};

// 1. Double every other digit back to front
// 2. If result of step 1 results in n > 9 replace with digital sum
// 3. summ all remaining numbers
// 4. if modulo 10 gives anything other than 0. Credit card is invalid