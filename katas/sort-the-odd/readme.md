<!-- -*- coding: windows-1252 -*- -->
# [Sort the odd](https://www.codewars.com/kata/578aa45ee9fd15ff4600090d)   
#### *Kyu*: **6**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/578aa45ee9fd15ff4600090d>