int get_real_floor(int n)
{
    if ( n > 13 ) return n-2;
    if ( n == 13 ) return 14;
    if (n <= 0) return n;
    return n-1;
}