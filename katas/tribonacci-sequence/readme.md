<!-- -*- coding: windows-1252 -*- -->
# [Tribonacci Sequence](https://www.codewars.com/kata/556deca17c58da83c00002db)   
#### *Kyu*: **6**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/556deca17c58da83c00002db>