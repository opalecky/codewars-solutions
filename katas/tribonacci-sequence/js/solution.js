function tribonacci(signature,n){
  var arr = [...signature];
  if( n > 3) {
    while (arr.length < n) {
      arr.push(arr[arr.length-3]+arr[arr.length-2]+arr[arr.length-1]);
    }
  } else {
    switch(n) {
      case 0: arr = []; break;
      case 1: arr = [arr[0]]; break;
      case 2: arr = [arr[0], arr[1]]; break;
    }
  }
  return arr;
}