<!-- -*- coding: windows-1252 -*- -->
# [parseInt() reloaded](https://www.codewars.com/kata/525c7c5ab6aecef16e0001a5)   
#### *Kyu*: **4**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/525c7c5ab6aecef16e0001a5>