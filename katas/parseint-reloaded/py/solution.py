from math import floor, log

Nums = {
    'zero': 0,
    'one': 1,
    'two': 2,
    'three': 3,
    'four': 4,
    'five': 5,
    'six': 6,
    'seven': 7,
    'eight': 8,
    'nine': 9,
    'ten': 10,
    'eleven': 11,
    'twelve': 12,
    'thirteen': 13,
    'fourteen': 14,
    'fifteen': 15,
    'sixteen': 16,
    'seventeen': 17,
    'eighteen': 18,
    'nineteen': 19,
    'twenty': 20,
    'thirty': 30,
    'forty': 40,
    'fifty': 50,
    'sixty': 60,
    'seventy': 70,
    'eighty': 80,
    'ninety': 90,
}
mults_of_ten = {
    'hundred': 100,
    'thousand': 1000,
    'million': 1_000_000
}


def breakdown(num):
    otp = [0, 0, 0, 0, 0, 0, 0]
    for k in reversed(range(len(otp))):
        mult = 10 ** k
        add = floor(num / mult)
        otp[k] += add
        num -= add * mult
    return otp


def sum_arrays(arr1, arr2):
    if len(arr1) != len(arr2):
        return Exception('arrays must be of same length!')
    for index, v1 in enumerate(arr1):
        arr1[index] += arr2[index]
        if arr1[index] % 10 and arr1[index] > 10:
            arr1[index + 1] += 1
            arr1[index] -= 10
    return arr1


def shift_array(arr, amount):
    l = len(arr)
    for i in range(amount):
        temp = [0]
        temp.extend(arr)
        while len(temp) != l:
            temp.pop()
        arr = temp
    return arr


def parse_int(string):
    print_zeros = False
    bd = [0, 0, 0, 0, 0, 0, 0]
    spl = string.split()
    words = []
    for i, w in enumerate(spl):
        if w in Nums.keys():
            bd = sum_arrays(bd, breakdown(Nums[w]))
        elif '-' in w:
            sub_split = w.split('-')
            for word in sub_split:
                br = breakdown(Nums[word]);
                bd = sum_arrays(bd, br)
        elif w in mults_of_ten.keys():
            if w == 'thousand':
                if i == len(spl):
                    bd = shift_array(bd, 3)
                else:
                    temp = 0
                    for i, key in enumerate(bd):
                        temp += key * (10**i)
                    words.append(str(temp))
                    bd = [0, 0, 0, 0, 0, 0, 0]
                    print_zeros = True
            elif w == 'million':
                bd = shift_array(bd, 6)
            else:
                bd = shift_array(bd, 2)

    if words:
        pp = ''
        otp = 0
        for i, key in enumerate(bd):
            otp += key * (10 ** i)
        if print_zeros:
            pp = '0'*(3-len(str(otp)))
            pp += str(otp)
        return int(''.join([words[0], pp]))
    else:
        otp = 0
        for i, key in enumerate(bd):
            otp += key * (10 ** i)
        return otp
