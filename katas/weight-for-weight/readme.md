<!-- -*- coding: windows-1252 -*- -->
# [Weight for weight](https://www.codewars.com/kata/55c6126177c9441a570000cc)   
#### *Kyu*: **5**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/55c6126177c9441a570000cc>