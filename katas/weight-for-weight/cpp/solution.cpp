#include <iostream>
#include <string>
#include <cstring>
#include <vector>
#include <algorithm>
#define LOG(x) std::cout << x <<std::endl;

class WeightSort
{
public:
    static std::string orderWeight(const std::string &strng);
};


static int char_to_int( char i ) {
    return std::atoi( std::string( 1, i ).c_str() );
}

int digital_root( long long unsigned int number ) {
    int output = 0;
    for ( auto i : std::to_string( number ) ) {
        output += char_to_int( i );
    }
    if ( strlen( std::to_string( output ).c_str() ) ) {
        return output;
    }
    return digital_root( output );
}

std::vector< long long unsigned int > split_to_ints( const std::string& input, const char separator ) {
    std::vector< long long unsigned int > otp;
    int last_separation = 0;
    for ( size_t i = 0; i < strlen( input.c_str() ); i ++ ) {
        if ( input[ i ] == separator || i == strlen( input.c_str() ) - 1 ) {
            long long int num = std::atoll( input.substr( last_separation, i ).c_str() );
//            LOG(num);
            otp.push_back( num );
            last_separation = i;
//            LOG(num);
        }
    }
    return otp;
}

template < typename T >
std::string join( std::vector< T > v, const char* connector ) {
    std::string otp;
    size_t counter = 0;
    for ( const auto i : v ) {
        otp += std::to_string( i );
        if ( counter != v.size() - 1 ) otp.append( connector );
        counter ++;
    }
    return otp;
}

std::string WeightSort::orderWeight( const std::string& strng ) {
    std::vector< long long unsigned int > split( split_to_ints( strng, ' ' ) );
    std::sort( split.begin(), split.end(), [ ]( long long unsigned int i1, long long unsigned int i2 ) {
        int w1 = digital_root( i1 );
        int w2 = digital_root( i2 );
        if ( w1 != w2 ) return w1 < w2;
        return std::to_string( i1 ) < std::to_string( i2 );
    } );
    return join( split, " " );
}