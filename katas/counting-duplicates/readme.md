<!-- -*- coding: windows-1252 -*- -->
# [Counting Duplicates](https://www.codewars.com/kata/54bf1c2cd5b56cc47f0007a1)   
#### *Kyu*: **6**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/54bf1c2cd5b56cc47f0007a1>