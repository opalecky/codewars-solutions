size_t duplicateCount(const std::string& in); // helper for tests


struct SymbolCount {
    char character;
    int count;
};

size_t duplicateCount( const char* in ) {
    int finalCount = 0;
    std::vector< SymbolCount > counting;
    for ( auto chara : (std::string) in ) {
        bool found = false;
        for ( auto &symbol : counting ) {
            if ( symbol.character == tolower( chara ) ) {
                symbol.count += 1;
                found = true;
                break;
            }
        }
        if ( ! found ) {
            counting.push_back( SymbolCount{ static_cast<char>(tolower( chara )), 1 } );
        }
    }
    for ( auto s : counting )
        if ( s.count > 1 ) finalCount ++;
    return finalCount;
}