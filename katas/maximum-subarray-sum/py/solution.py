def max_sequence(arr):
    if not len(arr) or max(arr) < 0:
        return 0
    
    # is a bit brute I know
    currentHighest = [0,0,0]
    for (index, i) in enumerate(arr):
        for j in range(index, len(arr)+1):
            sub = arr[index:j]
            if sum(sub) > currentHighest[2]:
                currentHighest = [index, j, sum(sub)]
                
    return currentHighest[2] 