<!-- -*- coding: windows-1252 -*- -->
# [Maximum subarray sum](https://www.codewars.com/kata/54521e9ec8e60bc4de000d6c)   
#### *Kyu*: **5**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/54521e9ec8e60bc4de000d6c>