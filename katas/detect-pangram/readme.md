<!-- -*- coding: windows-1252 -*- -->
# [Detect Pangram](https://www.codewars.com/kata/545cedaa9943f7fe7b000048)   
#### *Kyu*: **6**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/545cedaa9943f7fe7b000048>