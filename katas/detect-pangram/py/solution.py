import string

def is_pangram(s):
    alphabet = list("abcdefghjiklmnopqrstuvwxyz")
    for char in list(s):
        if char.lower() in alphabet:
            alphabet.remove(char.lower())
        
    return len(alphabet) == 0