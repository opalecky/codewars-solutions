using System;
using System.Collections.Generic;
using System.Linq;

public static class Kata
{
  public static bool IsPangram(string str)
  {
    
    List<Char> AllLetters = new List<Char>("abcdefghijklmnopqrstuvwxyz".ToCharArray());
    
    foreach (char c1 in str.ToCharArray()) {
      AllLetters = new List<Char>(AllLetters.Where(c => Char.ToLower(c) != Char.ToLower(c1)).ToArray());
    }
    
    return AllLetters.Capacity == 0;
  }
}