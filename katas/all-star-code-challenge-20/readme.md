<!-- -*- coding: windows-1252 -*- -->
# [All Star Code Challenge #20](https://www.codewars.com/kata/5865a75da5f19147370000c7)   
#### *Kyu*: **7**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/5865a75da5f19147370000c7>