#include <vector>
using namespace std;

template<class T>
vector<T> add_arrays(const vector<T>& array1, const vector<T>& array2) {
    if(array1.size() != array2.size()) throw std::exception();
    vector<T> out_Vector;
    out_Vector.reserve(array1.size()*sizeof(T));
    for (size_t i=0; i<array1.size(); i++) out_Vector.emplace_back(array1[i]+array2[i]);
    return out_Vector;
}
