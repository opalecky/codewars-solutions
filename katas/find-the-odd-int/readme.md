<!-- -*- coding: windows-1252 -*- -->
# [Find the odd int](https://www.codewars.com/kata/54da5a58ea159efa38000836)   
#### *Kyu*: **6**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/54da5a58ea159efa38000836>