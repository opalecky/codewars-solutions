#include <vector>

struct Inter {
  int val, count;
};

int findOdd(const std::vector<int>& numbers){
  std::vector<Inter> counters;
  
  for(auto i : numbers) {
    bool exists = false;
    for(auto &ctr:counters){
      if(ctr.val == i) {
        ctr.count++; 
      exists = true;
       }
    }
    if(exists) continue;
    counters.push_back({i, 1});
  }
  
  for(auto ctr : counters) {
//     std::cout << ctr.val << " appeared " << ctr.count << " times" << std::endl;
    if(ctr.count % 2) return ctr.val;
  }
  
  return 0;
  //your code here
}