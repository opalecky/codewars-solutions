def find_it(seq):
    numbers = set(seq)
    odd = None
    for num in numbers:
        if seq.count(num) % 2:
            return num
    return None
