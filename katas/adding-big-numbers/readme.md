<!-- -*- coding: windows-1252 -*- -->
# [Adding Big Numbers](https://www.codewars.com/kata/525f4206b73515bffb000b21)   
#### *Kyu*: **4**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/525f4206b73515bffb000b21>