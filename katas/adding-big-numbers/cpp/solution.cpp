#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include <algorithm>

#define LOG( x ) std::cout << x << std::endl;

std::vector< int > ParseStringIntoInt( const std::string& in_Str )
  {
    
    std::vector< int > out_NumVector;
    out_NumVector.reserve( in_Str.length() );
    for (char c_Char : in_Str )
      {
        out_NumVector.push_back( c_Char - '0' );
      }
    return out_NumVector;
  }

template < typename T >
std::vector< size_t > Range( T end )
  {
    
    std::vector< size_t > out_Vector;
    out_Vector.reserve( end );
    for ( size_t i = 0; i < end; i ++ ) out_Vector.push_back( i );
    return out_Vector;
  }

std::string add( const std::string& a, const std::string& b )
  {
    if(a.empty()) return b;
    if(b.empty()) return a;
    std::string out_String;
    std::vector< int > out_Vector;
    
    std::vector< int > m_viNum1 = ParseStringIntoInt( a ), m_viNum2 = ParseStringIntoInt( b );
    
    size_t m_NewSize = std::max( m_viNum1.size(), m_viNum2.size() );
    if ( m_viNum2.back() + m_viNum1.back() > 9 ) m_NewSize ++;
    
    out_Vector.reserve( m_NewSize );
    
    std::reverse( m_viNum1.begin(), m_viNum1.end() );
    std::reverse( m_viNum2.begin(), m_viNum2.end() );
    
    std::vector< int >& longer = ( m_viNum1.size() > m_viNum2.size() ? m_viNum1 : m_viNum2 );
    std::vector< int >& shorter = ( m_viNum1.size() <= m_viNum2.size() ? m_viNum1 : m_viNum2 );
    int m_Hold = 0;
    for ( auto i : Range( longer.size() ) )
      {
        int m_New = longer.at( i );
        m_New += i >= shorter.size() ? 0 : shorter.at(i);
        m_New += m_Hold;
        
        m_Hold = 0;
        while ( m_New > 9 )
          {
            m_Hold += 1;
            m_New -= 10;
          }
        out_String.append( std::to_string( m_New ) );
      }
    if ( m_Hold != 0 ) out_String.append( std::to_string( m_Hold ) );
    std::reverse( out_String.begin(), out_String.end() );
    return out_String;
  }
