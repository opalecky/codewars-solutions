<!-- -*- coding: windows-1252 -*- -->
# [Fold an array](https://www.codewars.com/kata/57ea70aa5500adfe8a000110)   
#### *Kyu*: **6**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/57ea70aa5500adfe8a000110>