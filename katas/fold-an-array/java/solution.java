public class Kata
{
  public static int[] foldArray(int[] array, int runs)
  {
    int half_length = (int) (Math.floor(array.length/2));
    int middle_element = array[half_length];
    int [] out_Array;
    if(array.length % 2 != 0) {
      out_Array = new int[half_length+1];
      out_Array[half_length] = middle_element;
    } else {
      out_Array = new int[half_length];
    }
    for(int i = 0; i < half_length; i++) {
      out_Array[i] = (array[i]+array[array.length-1-i]);
    }
    runs--;
    if(runs == 0) return out_Array;
    else return foldArray(out_Array, runs);
   }
}