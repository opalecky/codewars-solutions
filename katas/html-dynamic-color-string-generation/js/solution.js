const get_random_int_between = (min, max) => {
  return Math.round(Math.random()*(max-min)+min);
}
var generateColor = function() {
    const numbers = [
      get_random_int_between(0,255).toString(16),
      get_random_int_between(0,255).toString(16),
      get_random_int_between(0,255).toString(16), 
    ]
    strings = numbers.map(x=>'0'.repeat(2-x.length)+x)
    return `#${strings[0]}${strings[1]}${strings[2]}`;
};