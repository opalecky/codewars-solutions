<!-- -*- coding: windows-1252 -*- -->
# [HTML dynamic color string generation](https://www.codewars.com/kata/56f1c6034d0c330e4a001059)   
#### *Kyu*: **6**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/56f1c6034d0c330e4a001059>