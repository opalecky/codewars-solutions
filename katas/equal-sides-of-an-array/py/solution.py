def find_even_index(arr):
    ind = 0
    for num in arr:
        ls = sum(arr.copy()[0:ind])
        rs = sum(arr.copy()[ind+1:len(arr)])
        if ls == rs:
            return ind
        ind += 1

    return -1
    
    #your code here