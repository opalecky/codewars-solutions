<!-- -*- coding: windows-1252 -*- -->
# [Equal Sides Of An Array](https://www.codewars.com/kata/5679aa472b8f57fb8c000047)   
#### *Kyu*: **6**  
Solution by **Adam Opalecky**  
Description available here: <https://www.codewars.com/kata/5679aa472b8f57fb8c000047>