import json
import os
import re


def create_description(folder, name, link, kyu):
    if not os.path.isdir(folder):
        os.makedirs(folder)
    if not os.path.isfile(os.path.abspath(folder + "/readme.md")):
        with open(os.path.abspath(f"{folder}/readme.md"), "x") as f:
<<<<<<< create_kata.py
            f.writelines(f"""# [{name}]({link})\nSolution by Adam Opalecký\r\nDescription available here: <{link}>""")
=======
            f.writelines(f"""<!-- -*- coding: windows-1252 -*- -->\r\n# [{name}]({link})   \n#### *Kyu*: **{kyu}**  \nSolution by **Adam Opalecky**  \r\nDescription available here: <{link}>""")
>>>>>>> create_kata.py
    else:
        print("readme.md already exists")


def clean_name(orig_name):
    return re.sub(r"[^a-zA-Z0-9\s]", "", orig_name.lower()).replace(" ", "-")


def create_kata_folder(name):
    if not os.path.isdir(os.path.abspath(f"./{name}")):
        pass


def parse_kyu(string: str) -> int:
    if string.lower() == "retired":
        return -1
    return int(re.match(re.compile(r"([\d])", re.I | re.A | re.M), string).groups()[0])


def lang_name_parser(lang_name) -> (str, str, str):
    match lang_name.lower():
        case 'c++':
            return "cpp", "cpp", "C++"
        case 'c#':
            return "c-sharp", "cs", "C#"
        case 'javascript':
            return "js", "js", "JavaScript"
        case 'typescript':
            return "ts", "ts", "TypeScript"
        case 'java':
            return "java", "java", "Java"
        case 'kotlin':
            return "kt", "kt", "Kotlin"
        case 'python':
            return "py", "py", "Python"
        case 'rust':
            return "rs", "rs", "Rust"
        case 'php':
            return "php", "php", "PHP"
        case 'c':
            return "c", "c", "C"
    return "unknown", "unknown", "txt"


if __name__ == "__main__":
    with open(os.path.abspath("./scrape-data/solutions.json"), "r") as file:
        for solution in json.load(file):
            name = solution['name']
            link = solution['link']
            kyu = parse_kyu(solution['kyu'])
            solutions = solution["all_solutions"]

            create_kata_folder(clean_name(name))
            create_description(os.path.abspath(f"./katas/{clean_name(name)}"), name, link, kyu)

            for sol in solutions:
                try:
                    lang_dir, ext, lang_name = lang_name_parser(sol["language"])
                    if isinstance(sol["code"], list):
                        for index, version in enumerate(sol["code"]):
                            path = os.path.abspath(f"./katas/{clean_name(name)}/{lang_dir}")
                            os.makedirs(path)
                            file_name = f"solution_v1_{index}.{ext}"
                            sol_f = os.path.abspath(path + "/" + file_name)
                            mode = "x" if not os.path.isfile(sol_f) else "w"
                            with open(sol_f, mode) as w_file:
                                w_file.writelines(version)
                    else:
                        path = os.path.abspath(f"./katas/{clean_name(name)}/{lang_dir}")
                        os.makedirs(path)
                        file_name = f"solution.{ext}"
                        sol_f = os.path.abspath(path + "/" + file_name)
                        mode = "x" if not os.path.isfile(sol_f) else "w"
                        with open(sol_f, mode) as w_file:
                            w_file.writelines(sol["code"])
                except:
                    print("error")
            # print(solutions)
# print(clean_name("Ahoj, kurva macz"))
