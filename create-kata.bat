@echo off

cd D:\Documents\Dev\codewars-solutions\katas
md %1;
timeout 1 > NUL;
cd %1;
echo "" > description.md;
timeout 1 > NUL;
md %2;
timeout 1 > NUL;
cd %2;
echo "" > solution.%2;
timeout 1 > NUL;
notepad solution.%2;